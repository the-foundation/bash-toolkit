_gitlab_pipeline_cleanup() {
(which jq &>/dev/null && which curl &>/dev/null ) || echo "MISSING: "$(which jq &>/dev/null || echo jq)" "$(which curl &>/dev/null || echo curl)
echo "GITLAB PIPELINE CLEANUP TOOL:DELETING ALL EXCEPT RUNNING AND THE MOST RECENT PIPELINE"
if [ -z $GITLAB_URL ]; then echo GITLAB_URL:; read GITLAB_URL;fi
if [ -z $GITLAB_TOKEN ]; then echo TOKEN:; read GITLAB_TOKEN;fi
if [ -z $PROJECT ]; then echo PROJECT_NUM:; read PROJECT;fi

list=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$PROJECT/pipelines?per_page=100" |jq -c '.[] | select( .status != "running" )| .id ' |tail -n+2 |grep -v ^$)

echo -n "CLEANING PIPELINES from $GITLAB_URL Project # $PROJECT ( runs found on first page: "$(echo -n "$list" |wc -c  )" )"

while echo $(echo -n "$list" |wc -c  ) |grep -vq ^0$; do 
     echo -n "| page_pipes: "$(echo -n "$list" |wc -c  )"|"
     for item in $list ;do 
         echo -n "| -p $item |"
         curl -s  --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --request "DELETE" "$GITLAB_URL/api/v4/projects/$PROJECT/pipelines/$item"
     done 
     list=$(curl  -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$PROJECT/pipelines?per_page=100" |jq -c '.[] | select( .status != "running" )| .id ' |grep -v ^$)
done 
echo ; } ;

_gitlab_pipeline_cleanup_multi() {
    echo "GITLAB PIPELINE CLEANUP TOOL:DELETING ALL EXCEPT RUNNING AND THE MOST RECENT PIPELINE"
    if [ -z $GITLAB_URL ];    then echo GITLAB_URL:  ; read GITLAB_URL    ; export GITLAB_URL=${GITLAB_URL} ;fi
    if [ -z $GITLAB_TOKEN ]; then echo TOKEN:       ; read GITLAB_TOKEN ; export GITLAB_TOKEN=${GITLAB_TOKEN} ;fi
    for curproject in $@;do
         export PROJECT=${curproject}
        _gitlab_pipeline_cleanup $project ;
    done
}
